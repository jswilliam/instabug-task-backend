class CreateBugs < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string :device
      t.string :os
      t.integer :memory
      t.integer :storage

      t.timestamps
    end
    
    create_table :bugs do |t|
      t.string :application_token
      t.integer :number
      t.string :status
      t.string :priority
      t.text :comment
      t.references :state, foreign_key: true

      t.timestamps
    end
  end
end
