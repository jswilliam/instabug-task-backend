class State < ApplicationRecord
  has_one :bug
  validates_presence_of :device, :os, :memory, :storage
end
