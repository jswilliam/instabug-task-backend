require 'elasticsearch/model'
class Bug < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :state
  validates_presence_of :status, :priority, :application_token
  validates_inclusion_of :status, in: [ 'new', 'in_progress', 'closed'], message: 'Status must be new, in_progress or closed'
  validates_inclusion_of :priority, in: [ 'minor', 'major', 'critical'], message: 'Status must be minor, major or critical'
end
Bug.import force: true
