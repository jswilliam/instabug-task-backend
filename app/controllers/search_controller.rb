class SearchController < ApplicationController
  def search
    if params[:q].nil?
      @bugs = []
    else
      @bugs = Bug.search params[:q]
    end
    respond_to do |format|
      format.html
      format.json { render json: @bugs }
    end
  end
end
