class BugsController < ApplicationController
  before_action :set_bug, only: [ :edit, :update, :destroy]

  # GET /bugs
  # GET /bugs.json
  def index
    @bugs = Bug.where(application_token: request.headers["HTTP_APPLICATION_TOKEN"])
  end

  # GET /bugs/1
  # GET /bugs/1.json
  def show
    @bug = Bug.find_by(number: params[:id], application_token: request.headers["HTTP_APPLICATION_TOKEN"])
  end

  # GET /bugs/new
  def new
    @bug = Bug.new
  end


  # POST /bugs
  # POST /bugs.json
  def create
    @bug = Bug.new(bug_params)
    set_number
    respond_to do |format|
      if @bug.save
        format.html { render :index }
        format.json { render json: @bug, status: :ok}
      else
        format.html { render :new }
        format.json { render json: @bug.errors, status: :unprocessable_entity }
      end
    end
  end



  private
  # Use callbacks to share common setup or constraints between actions.
  def set_bug
    @bug = Bug.find(params[:id])
  end

  def set_number
    @bugs = Bug.where(application_token: params[:bug][:application_token])
    if !@bugs.empty?
      @bug.number = @bugs.maximum('number') + 1
    else
      @bug.number = 1;
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bug_params
    params.fetch(:bug).permit(:application_token, :number, :status, :priority, :comment, :state_id)
  end
end
